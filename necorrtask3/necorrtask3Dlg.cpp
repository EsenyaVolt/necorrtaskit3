﻿
// necorrtask3Dlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "necorrtask3.h"
#include "necorrtask3Dlg.h"
#include "afxdialogex.h"
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <iomanip>
#include <random>
#include <cassert>

#define PI 3.1415926535

using namespace std;

#define DOTS(x,y) (xpx*((x)-xminx)),(ypx*((y)-ymaxx)) // макрос перевода координат для графикa
#define DOTSSOBSTV(x,y) (xpy*((x)-xminy)),(ypy*((y)-ymaxy)) // макрос перевода координат для графика
#define DOTSSV(x,y) (xpsv*((x)-xminsv)),(ypsv*((y)-ymaxsv)) // макрос перевода координат для графика

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Диалоговое окно Cnecorrtask3Dlg



Cnecorrtask3Dlg::Cnecorrtask3Dlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_NECORRTASK3_DIALOG, pParent)
	, M_razm(3)
	, N_razm(3)
	, determ(0)
	, result_part(_T(""))
	, nevyaz_obr(0)
	, nevyaz_psevdo(0)
	, A1(0.05)
	, A2(0.01)
	, A3(0.02)
	, omega1(0.05)
	, omega2(0.04)
	, omega3(0.00006)
	, phase1(0)
	, phase2(0)
	, phase3(0.4)
	, sigma1(15)
	, sigma2(25)
	, sigma3(50)
	, t1(25)
	, t2(250)
	, t3(400)
	, poryadokAKM(75)
	, t_sign(512)
	, number(1)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Cnecorrtask3Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT3, M_razm);
	DDX_Text(pDX, IDC_EDIT4, N_razm);
	DDX_Text(pDX, IDC_EDIT5, determ);
	DDX_Text(pDX, IDC_RESULT, result_part);
	DDX_Text(pDX, IDC_EDIT6, nevyaz_obr);
	DDX_Text(pDX, IDC_EDIT24, nevyaz_psevdo);
	DDX_Text(pDX, IDC_EDIT7, A1);
	DDX_Text(pDX, IDC_EDIT8, A2);
	DDX_Text(pDX, IDC_EDIT9, A3);
	DDX_Text(pDX, IDC_EDIT10, omega1);
	DDX_Text(pDX, IDC_EDIT11, omega2);
	DDX_Text(pDX, IDC_EDIT12, omega3);
	DDX_Text(pDX, IDC_EDIT13, phase1);
	DDX_Text(pDX, IDC_EDIT14, phase2);
	DDX_Text(pDX, IDC_EDIT15, phase3);
	DDX_Text(pDX, IDC_EDIT16, sigma1);
	DDX_Text(pDX, IDC_EDIT17, sigma2);
	DDX_Text(pDX, IDC_EDIT18, sigma3);
	DDX_Text(pDX, IDC_EDIT19, t1);
	DDX_Text(pDX, IDC_EDIT20, t2);
	DDX_Text(pDX, IDC_EDIT21, t3);
	DDX_Text(pDX, IDC_EDIT22, poryadokAKM);
	DDX_Text(pDX, IDC_EDIT23, t_sign);
	DDX_Control(pDX, IDC_RADIO1, polygarm);
	DDX_Control(pDX, IDC_RADIO2, gaussov);
	DDX_Control(pDX, IDC_RADIO3, expZatuh);
	DDX_Text(pDX, IDC_EDIT25, number);
}

BEGIN_MESSAGE_MAP(Cnecorrtask3Dlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_REZULT, &Cnecorrtask3Dlg::OnBnClickedRezult)
	ON_BN_CLICKED(IDC_REZULT2, &Cnecorrtask3Dlg::OnBnClickedSignal)
	ON_BN_CLICKED(IDC_RESHENIE, &Cnecorrtask3Dlg::OnBnClickedReshenie)
END_MESSAGE_MAP()


// Обработчики сообщений Cnecorrtask3Dlg

BOOL Cnecorrtask3Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию
	PicWndSign = GetDlgItem(IDC_SIGNAL);
	PicDcSign = PicWndSign->GetDC();
	PicWndSign->GetClientRect(&PicSign);

	PicWndSobstv = GetDlgItem(IDC_SOBSTVZNACH);
	PicDcSobstv = PicWndSobstv->GetDC();
	PicWndSobstv->GetClientRect(&PicSobstv);

	PicWndSv = GetDlgItem(IDC_SOBSTVV);
	PicDcSv = PicWndSv->GetDC();
	PicWndSv->GetClientRect(&PicSv);

	CButton* pcb1 = (CButton*)(this->GetDlgItem(IDC_RADIO1));
	pcb1->SetCheck(1);

	// перья
	setka_pen.CreatePen(		//для сетки
		PS_DOT,					//пунктирная
		1,						//толщина 1 пиксель
		RGB(0, 0, 0));			//цвет  черный

	osi_pen.CreatePen(			//координатные оси
		PS_SOLID,				//сплошная линия
		3,						//толщина 3 пикселя
		RGB(0, 0, 0));			//цвет черный

	signal_pen.CreatePen(			//график исходного сигнала
		PS_SOLID,				//сплошная линия
		2,						//толщина 2 пикселя
		RGB(54, 6, 156));			//цвет синий

	signal2_pen.CreatePen(			//график исходного сигнала
		PS_SOLID,				//сплошная линия
		2,						//толщина 2 пикселя
		RGB(150, 10, 50));			//цвет синий

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void Cnecorrtask3Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		PererisovkaX(mnx, mxx);
		PererisovkaY(mny, mxy);
		PererisovkaSV(mny, mxy);
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR Cnecorrtask3Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

struct PRNG
{
	mt19937 engine;
};

void initGenerator(PRNG& generator)
{
	// Создаём псевдо-устройство для получения случайного зерна.
	random_device device;
	// Получаем случайное зерно последовательности
	generator.engine.seed(device());
} 

double getRandomFloat(PRNG& generator, double minValue, double maxValue)
{
	// Проверяем корректность аргументов
	assert(minValue < maxValue);

	// Создаём распределение
	uniform_real_distribution<double> distribution(minValue, maxValue);

	// Вычисляем псевдослучайное число: вызовем распределение как функцию,
	//  передав генератор произвольных целых чисел как аргумент.
	return distribution(generator.engine);
}

// Вычисляет определитель матрицы T размерностью N
double Cnecorrtask3Dlg::det(double** T, UINT32 N)
{
	double det__;
	int sub_j, s;
	double** subT;    // Субматрица как набор ссылок на исходную матрицу
	switch (N)
	{
	case 1:
		return T[0][0];
	case 2:
		return T[0][0] * T[1][1] - T[0][1] * T[1][0];
	default:
		if (N < 1)  // Некорректная матрица
		{
			MessageBox(L"Некорректная матрица!", L"Ошибка", MB_ICONERROR | MB_OK);
		}
		subT = new double* [N - 1];  // Массив ссылок на столбцы субматрицы
		det__ = 0;
		s = 1;        // Знак минора
		for (int i = 0; i < N; i++)  // Разложение по первому столбцу
		{
			sub_j = 0;
			for (int j = 0; j < N; j++)// Заполнение субматрицы ссылками на исходные столбцы
				if (i != j)      // исключить i строку
					subT[sub_j++] = T[j] + 1;  // здесь + 1 исключает первый столбец

			det__ = det__ + s * T[i][0] * det(subT, N - 1);
			s = -s;
		};
		delete[] subT;
		return det__;
	};
}

bool inverse(double** matrix, double** result, int size)
{
	// Изначально результирующая матрица является единичной
	// Заполняем единичную матрицу
	for (int i = 0; i < size; ++i)
	{
		for (int j = 0; j < size; ++j)
			result[i][j] = 0.0;

		result[i][i] = 1.0;
	}

	// Копия исходной матрицы
	double** copy = new double* [size]();

	// Заполняем копию исходной матрицы
	for (int i = 0; i < size; ++i)
	{
		copy[i] = new double[size];

		for (int j = 0; j < size; ++j)
			copy[i][j] = matrix[i][j];
	}

	// Проходим по строкам матрицы (назовём их исходными) сверху вниз.
	// На данном этапе происходит прямой ход
	// и исходная матрица превращается в верхнюю треугольную
	for (int k = 0; k < size; ++k)
	{
		// Если элемент на главной диагонали в исходной строке - нуль,
		// то ищем строку, где элемент того же столбца не нулевой,
		//  и меняем строки местами
		if (fabs(copy[k][k]) < 1e-8)
		{
			// Ключ, говорящий о том, что был произведён обмен строк
			bool changed = false;

			// Идём по строкам, расположенным ниже исходной
			for (int i = k + 1; i < size; ++i)
			{
				// Если нашли строку, где в том же столбце имеется ненулевой элемент
				if (fabs(copy[i][k]) > 1e-8)
				{
					// Меняем найденную и исходную строки местами
					// как в исходной матрице, так и в единичной
					swap(copy[k], copy[i]);
					swap(result[k], result[i]);

					// Взводим ключ - сообщаем о произведённом обмене строк
					changed = true;

					break;
				}
			}

			// Если обмен строк произведён не был - матрица не может быть обращена
			if (!changed)
			{
				// Чистим память
				for (int i = 0; i < size; ++i)
					delete[] copy[i];

				delete[] copy;

				// Сообщаем о неудаче обращения
				return false;
			}
		}

		// Запоминаем делитель - диагональный элемент
		double div = copy[k][k];

		// Все элементы исходной строки делим на диагональный элемент 
		// как в исходной матрице, так и в единичной
		for (int j = 0; j < size; ++j)
		{
			copy[k][j] /= div;
			result[k][j] /= div;
		}

		// Идём по строкам, которые расположены ниже исходной
		for (int i = k + 1; i < size; ++i)
		{
			// Запоминаем множитель - элемент очередной строки,
			// расположенный под диагональным элементом исходной строки
			double multi = copy[i][k];

			// Отнимаем от очередной строки исходную, умноженную
			// на сохранённый ранее множитель 
			// как в исходной, так и в единичной матрице
			for (int j = 0; j < size; ++j)
			{
				copy[i][j] -= multi * copy[k][j];
				result[i][j] -= multi * result[k][j];
			}
		}
	}

	// Проходим по вернхней треугольной матрице, полученной на прямом ходе, снизу вверх
	// На данном этапе происходит обратный ход, и из исходной матрицы 
	//окончательно формируется единичная, а из единичной - обратная
	for (int k = size - 1; k > 0; --k)
	{
		// Идём по строкам, которые расположены выше исходной
		for (int i = k - 1; i + 1 > 0; --i)
		{
			// Запоминаем множитель - элемент очередной строки,
			// расположенный над диагональным элементом исходной строки
			double multi = copy[i][k];

			// Отнимаем от очередной строки исходную, умноженную
			// на сохранённый ранее множитель 
			// как в исходной, так и в единичной матрице
			for (int j = 0; j < size; ++j)
			{
				copy[i][j] -= multi * copy[k][j];
				result[i][j] -= multi * result[k][j];
			}
		}
	}

	// Чистим память
	for (int i = 0; i < size; ++i)
		delete[] copy[i];

	delete[] copy;

	// Сообщаем об успехе обращения
	return true;
}

int Cnecorrtask3Dlg::svd_hestenes(int m_m, int n_n, double* a, double* u, double* v, double* sigma)
{
	double thr = 1.E-4f, nul = 1.E-16f;
	int n, m, i, j, l, k, lort, iter, in, ll, kk;
	double alfa, betta, hamma, eta, t, cos0, sin0, buf, s;
	n = n_n;
	m = m_m;
	for (i = 0; i < n; i++)
	{
		in = i * n;
		for (j = 0; j < n; j++)
			if (i == j) v[in + j] = 1.;
			else v[in + j] = 0.;
	}
	for (i = 0; i < m; i++)
	{
		in = i * n;
		for (j = 0; j < n; j++)
		{
			u[in + j] = a[in + j];
		}
	}

	iter = 0;
	while (1)
	{
		lort = 0;
		iter++;
		for (l = 0; l < n - 1; l++)
			for (k = l + 1; k < n; k++)
			{
				alfa = 0.; betta = 0.; hamma = 0.;
				for (i = 0; i < m; i++)
				{
					in = i * n;
					ll = in + l;
					kk = in + k;
					alfa += u[ll] * u[ll];
					betta += u[kk] * u[kk];
					hamma += u[ll] * u[kk];
				}

				if (sqrt(alfa * betta) < nul)	continue;
				if (fabs(hamma) / sqrt(alfa * betta) < thr) continue;

				lort = 1;
				eta = (betta - alfa) / (2.f * hamma);
				t = (double)((eta / fabs(eta)) / (fabs(eta) + sqrt(1. + eta * eta)));
				cos0 = (double)(1. / sqrt(1. + t * t));
				sin0 = t * cos0;

				for (i = 0; i < m; i++)
				{
					in = i * n;
					buf = u[in + l] * cos0 - u[in + k] * sin0;
					u[in + k] = u[in + l] * sin0 + u[in + k] * cos0;
					u[in + l] = buf;

					if (i >= n) continue;
					buf = v[in + l] * cos0 - v[in + k] * sin0;
					v[in + k] = v[in + l] * sin0 + v[in + k] * cos0;
					v[in + l] = buf;
				}
			}

		if (!lort) break;
	}

	for (i = 0; i < n; i++)
	{
		s = 0.;
		for (j = 0; j < m; j++)	s += u[j * n + i] * u[j * n + i];
		s = (double)sqrt(s);
		sigma[i] = s;
		if (s < nul)	continue;
		for (j = 0; j < m; j++)	u[j * n + i] /= s;
	}
	//======= Sortirovka ==============
	for (i = 0; i < n - 1; i++)
		for (j = i; j < n; j++)
			if (sigma[i] < sigma[j])
			{
				s = sigma[i]; sigma[i] = sigma[j]; sigma[j] = s;
				for (k = 0; k < m; k++)
				{
					s = u[i + k * n]; u[i + k * n] = u[j + k * n]; u[j + k * n] = s;
				}
				for (k = 0; k < n; k++)
				{
					s = v[i + k * n]; v[i + k * n] = v[j + k * n]; v[j + k * n] = s;
				}
			}

	return iter;
}

void Cnecorrtask3Dlg::PererisovkaX(double y_min, double y_max)
{
	PicDcSign->FillSolidRect(&PicSign, RGB(250, 250, 250));			//закрашиваю фон 

	//ГРАФИК СИГНАЛА

	//область построения
	xminx = -32;			//минимальное значение х
	xmaxx = t_sign;			//максимальное значение х
	yminx = y_min * 1.25;			//минимальное значение y
	if (gaussov.GetCheck())
	{
		yminx = -0.15 * y_max;
	}
	if (expZatuh.GetCheck())
	{
		xminx = -40;
	}
	ymaxx = y_max * 1.25;		//максимальное значение y

	xpx = ((double)(PicSign.Width()) / (xmaxx - xminx));			//Коэффициенты пересчёта координат по Х
	ypx = -((double)(PicSign.Height()) / (ymaxx - yminx));			//Коэффициенты пересчёта координат по У

	PicDcSign->SelectObject(&osi_pen);		//выбираем перо

	//создаём Ось Y
	PicDcSign->MoveTo(DOTS(0, ymaxx));
	PicDcSign->LineTo(DOTS(0, yminx));
	//создаём Ось Х
	PicDcSign->MoveTo(DOTS(xminx, 0));
	PicDcSign->LineTo(DOTS(xmaxx, 0));

	//подпись осей
	PicDcSign->TextOutW(DOTS(5, ymaxx - 0.5), _T("S"));
	PicDcSign->TextOutW(DOTS(xmaxx - 5, 1), _T("t"));

	PicDcSign->SelectObject(&setka_pen);

	//отрисовка сетки по х
	for (double x = 0; x <= xmaxx; x += xmaxx * 0.12)
	{
		PicDcSign->MoveTo(DOTS(x, ymaxx));
		PicDcSign->LineTo(DOTS(x, yminx));
	}
	//отрисовка сетки по у
	for (double y = yminx; y <= ymaxx; y += ymaxx * 0.24)
	{
		PicDcSign->MoveTo(DOTS(xminx, y));
		PicDcSign->LineTo(DOTS(xmaxx, y));
	}

	//подпись точек на оси
	CFont font;
	font.CreateFontW(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	PicDcSign->SelectObject(font);

	//по Y с шагом 
	for (double i = yminx; i <= ymaxx; i += ymaxx * 0.12)
	{
		CString str;
		if (expZatuh.GetCheck())
		{
			str.Format(_T("%.3f"), i);
			PicDcSign->TextOutW(DOTS(-38, i), str);
		}
		else
		{
			str.Format(_T("%.2f"), i);
			PicDcSign->TextOutW(DOTS(-30, i), str);
		}
	}

	//по X с шагом 
	for (double j = xmaxx * 0.12; j <= xmaxx; j += xmaxx * 0.12)
	{
		CString str;
		str.Format(_T("%.0f"), j);
		PicDcSign->TextOutW(DOTS(j, -0.01 * ymaxx), str);
		
	}
}

void Cnecorrtask3Dlg::PererisovkaY(double y_min, double y_max)
{
	//ГРАФИК 2
	PicDcSobstv->FillSolidRect(&PicSobstv, RGB(250, 250, 250));			//закрашиваю фон

	//область построения

	xminy = -5;			//минимальное значение х
	xmaxy = poryadokAKM;			//максимальное значение х
	yminy = y_min * 1.25;			//минимальное значение y
	ymaxy = y_max * 1.25;		//максимальное значение y

	xpy = ((double)(PicSobstv.Width()) / (xmaxy - xminy));			//Коэффициенты пересчёта координат по Х
	ypy = -((double)(PicSobstv.Height()) / (ymaxy - yminy));			//Коэффициенты пересчёта координат по У

	PicDcSobstv->SelectObject(&osi_pen);		//выбираем перо

	//создаём Ось Y
	PicDcSobstv->MoveTo(DOTSSOBSTV(0, ymaxy));
	PicDcSobstv->LineTo(DOTSSOBSTV(0, yminy));
	//создаём Ось Х
	PicDcSobstv->MoveTo(DOTSSOBSTV(xminy, 0));
	PicDcSobstv->LineTo(DOTSSOBSTV(xmaxy, 0));

	//подпись осей
	PicDcSobstv->TextOutW(DOTSSOBSTV(15, ymaxy - 0.5), _T("A"));
	PicDcSobstv->TextOutW(DOTSSOBSTV(xmaxy - 20, 1), _T("t"));


	PicDcSobstv->SelectObject(&setka_pen);

	//отрисовка сетки по х
	for (double x = xminy; x <= xmaxy; x += xmaxy * 0.12)
	{
		PicDcSobstv->MoveTo(DOTSSOBSTV(x, ymaxy));
		PicDcSobstv->LineTo(DOTSSOBSTV(x, yminy));
	}
	//отрисовка сетки по у
	for (double y = yminy; y <= ymaxy; y += ymaxy * 0.24)
	{
		PicDcSobstv->MoveTo(DOTSSOBSTV(xminy, y));
		PicDcSobstv->LineTo(DOTSSOBSTV(xmaxy, y));
	}


	//подпись точек на оси
	CFont fonty;
	fonty.CreateFontW(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	PicDcSobstv->SelectObject(fonty);

	//по Y с шагом 10
	for (double i = yminy; i <= ymaxy; i += ymaxy * 0.12)
	{
		CString str;
		str.Format(_T("%.3f"), i);
		PicDcSobstv->TextOutW(DOTSSOBSTV(-5, i), str);
	}
	//по X с шагом 25
	for (double j = xmaxy * 0.12; j <= xmaxy; j += xmaxy * 0.12)
	{
		CString str;
		str.Format(_T("%.0f"), j);
		PicDcSobstv->TextOutW(DOTSSOBSTV(j, -0.001), str);
	}
}

void Cnecorrtask3Dlg::PererisovkaSV(double y_min, double y_max)
{
	//ГРАФИК 2
	PicDcSv->FillSolidRect(&PicSv, RGB(250, 250, 250));			//закрашиваю фон

	//область построения

	xminsv = -6;			//минимальное значение х
	xmaxsv = poryadokAKM;			//максимальное значение х
	yminsv = y_min * 1.25;			//минимальное значение y
	ymaxsv = y_max * 1.25;		//максимальное значение y

	xpsv = ((double)(PicSv.Width()) / (xmaxsv - xminsv));			//Коэффициенты пересчёта координат по Х
	ypsv = -((double)(PicSv.Height()) / (ymaxsv - yminsv));			//Коэффициенты пересчёта координат по У

	PicDcSv->SelectObject(&osi_pen);		//выбираем перо

	//создаём Ось Y
	PicDcSv->MoveTo(DOTSSV(0, ymaxsv));
	PicDcSv->LineTo(DOTSSV(0, yminsv));
	//создаём Ось Х
	PicDcSv->MoveTo(DOTSSV(xminsv, 0));
	PicDcSv->LineTo(DOTSSV(xmaxsv, 0));

	//подпись осей
	PicDcSv->TextOutW(DOTSSV(15, ymaxsv - 0.5), _T("A"));
	PicDcSv->TextOutW(DOTSSV(xmaxsv - 20, 1), _T("t"));

	PicDcSv->SelectObject(&setka_pen);

	//отрисовка сетки по х
	for (double x = xminsv; x <= xmaxsv; x += xmaxsv * 0.12)
	{
		PicDcSv->MoveTo(DOTSSV(x, ymaxsv));
		PicDcSv->LineTo(DOTSSV(x, yminsv));
	}
	//отрисовка сетки по у
	for (double y = yminsv; y <= ymaxsv; y += ymaxsv * 0.24)
	{
		PicDcSv->MoveTo(DOTSSV(xminsv, y));
		PicDcSv->LineTo(DOTSSV(xmaxsv, y));
	}

	//подпись точек на оси
	CFont fontsv;
	fontsv.CreateFontW(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	PicDcSv->SelectObject(fontsv);

	//по Y с шагом 10
	for (double i = yminsv; i <= ymaxsv; i += ymaxsv * 0.12)
	{
		CString str;
		str.Format(_T("%.3f"), i);
		PicDcSv->TextOutW(DOTSSV(-6, i), str);
	}
	//по X с шагом 25
	for (double j = xmaxsv * 0.12; j <= xmaxsv; j += xmaxsv * 0.12)
	{
		CString str;
		str.Format(_T("%.0f"), j);
		PicDcSv->TextOutW(DOTSSV(j, -0.001), str);
	}
}

void Cnecorrtask3Dlg::Mashtab(double arr[], int dim, double* mmin, double* mmax)		//определяем функцию масштабирования
{
	*mmin = *mmax = arr[0];

	for (int i = 0; i < dim; i++)
	{
		if (*mmin > arr[i]) *mmin = arr[i];
		if (*mmax < arr[i]) *mmax = arr[i];
	}
}

double Cnecorrtask3Dlg::PolySignal(int t)
{
	double Amplitude[] = { A1, A2, A3 };
	double Frequency[] = { omega1, omega2, omega3 };
	double Phase[] = { phase1, phase2, phase3 };
	double result = 0.;

	for (int i = 0; i <= 2; i++)
	{
		result += Amplitude[i] * sin(2 * PI * Frequency[i] * t + Phase[i]);
	}

	return result;
}

double Cnecorrtask3Dlg::GaussSignal(int t)
{
	double Amplitude[] = { A1, A2, A3 };
	double Sigma[] = { sigma1, sigma2, sigma3 };
	double Centers_of_Gaussian_domes[] = { t1, t2, t3 };
	double result = 0.;

	for (int i = 0; i <= 2; i++)
	{
		result += Amplitude[i] * exp(-((t - Centers_of_Gaussian_domes[i]) / Sigma[i]) * ((t - Centers_of_Gaussian_domes[i]) / Sigma[i]));
	}

	return result;
}

double Cnecorrtask3Dlg::ZatuxSignal(int t)
{
	double Amplitude[] = { A1, A2, A3 };
	double Frequency[] = { omega1, omega2, omega3 };
	double Phase[] = { phase1, phase2, phase3 };
	double result = 0.;

	for (int i = 0; i <= 2; i++)
	{
		result += exp(-Amplitude[i] * t) * sin(2 * PI * Frequency[i] * t + Phase[i]);
	}

	return result;
}

void Cnecorrtask3Dlg::OnBnClickedRezult()
{
	// TODO: добавьте свой код обработчика уведомлений
	UpdateData(TRUE);

	PRNG generator;
	initGenerator(generator);

	double** A = new double* [N_razm];
	for (int i = 0; i < N_razm; i++)
	{
		A[i] = new double[M_razm];
	}

	double** Aobr = new double* [N_razm];
	for (int i = 0; i < N_razm; i++)
	{
		Aobr[i] = new double[M_razm];
	}

	double* b = new double[N_razm];
	double* x = new double[N_razm];
	double* x_psevdo = new double[M_razm];

	for (int i = 0; i < N_razm; i++)
	{
		b[i] = 0;
		x[i] = 0;
		x_psevdo[i] = 0;
		for (int j = 0; j < M_razm; j++)
		{
			A[i][j] = 0;
			Aobr[i][j] = 0;
		}
	}

	A[0][0] = 1.01;
	A[0][1] = 2.01;
	A[0][2] = 3.01;
	A[1][0] = 4.01;
	A[1][1] = 5.01;
	A[1][2] = 6.01;
	A[2][0] = 7.01;
	A[2][1] = 8.01;
	A[2][2] = 9.01;

	for (int i = 0; i < M_razm; i++)
	{
		b[i] = getRandomFloat(generator, -1, 1);
	}

	ofstream out("result.txt");

	out << "\tИНФОРМАЦИОННЫЕ ТЕХНОЛОГИИ" << endl;
	out << "\tЗадание 3. Построение и визуализация базиса Каруена-Лоэва. Часть 1." << endl;

	out << "\n\nМатрица коэффициентов А:" << endl;
	for (int i = 0; i < N_razm; i++)
	{
		for (int j = 0; j < M_razm; j++)
		{
			out << A[i][j] << "\t";
		}
		out << "\n";
	}

	double determinant = det(A, N_razm);
	determ = determinant;
	//out << "\nОпределитель матрицы: " << determinant;

	out << "\n\nВектор свободных членов: " << endl;
	for (int i = 0; i < N_razm; i++)
	{
		out << setprecision(3) << b[i] << "  ";
	}

	out << "\n\n******** ОБРАТНАЯ МАТРИЦА ********" << endl << "****** (КЛАССИЧЕСКИЙ МЕТОД) ******";

	if (determinant != 0)
	{
		//транспонирование А
		inverse(A, Aobr, N_razm);

		out << "\n\nОбратная матрица к матрице А:\n\n";
		for (int i = 0; i < N_razm; i++)
		{
			for (int j = 0; j < M_razm; j++)
			{
				out << setprecision(3) << Aobr[i][j] << "\t";
			}
			out << "\n";
		}

		for (int i = 0; i < N_razm; i++)
		{
			for (int j = 0; j < M_razm; j++)
			{
				x[i] += Aobr[i][j] * b[j];
			}
		}
	}

	//решение методом псевдообратной матрицы Мура-Пенроуза

	double* MatrA = new double[N_razm * M_razm];
	double* MatrU = new double[N_razm * M_razm];
	double* MatrV = new double[N_razm * M_razm];
	double* VectorSigm = new double[N_razm];
	double* MatrSigm = new double[N_razm * M_razm];
	double* TranspMatrSigm = new double[N_razm * M_razm];

	double* Buffer = new double[N_razm * M_razm];
	double* Buffer1 = new double[N_razm];
	double* Buffer2 = new double[N_razm * M_razm];
	double* Buffer3 = new double[N_razm * M_razm];

	for (int i = 0; i < N_razm; i++)
	{
		VectorSigm[i] = 0;
		Buffer1[i] = 0;
		for (int j = 0; j < N_razm; j++)
		{
			MatrA[i * N_razm + j] = A[i][j];
			MatrU[i * N_razm + j] = 0;
			MatrV[i * N_razm + j] = 0;
			MatrSigm[i * N_razm + j] = 0;
			TranspMatrSigm[i * N_razm + j] = 0;
			Buffer[i * N_razm + j] = 0;
			Buffer2[i * N_razm + j] = 0;
			Buffer3[i * N_razm + j] = 0;
		}
	}

	out << "\n****** ПСЕВДООБРАТНАЯ МАТРИЦА (СВД) ******";

	svd_hestenes(N_razm, M_razm, MatrA, MatrU, MatrV, VectorSigm);

	float porog = VectorSigm[0] / 1000;

	//транспонируем матрицу U
	for (int i = 0; i < N_razm; i++)
	{
		for (int j = 0; j < M_razm; j++)
		{
			Buffer[i * M_razm + j] = MatrU[i + j * M_razm];
		}
	}

	for (int i = 0; i < N_razm; i++)
	{
		for (int j = 0; j < M_razm; j++)
		{
			if (i == j)
				MatrSigm[i * M_razm + j] = VectorSigm[i];
			else
				MatrSigm[i * M_razm + j] = 0;
		}
	}

	//транспонируем матрицу Sigma
	for (int i = 0; i < N_razm; i++)
	{
		if (VectorSigm[i] <= porog)
			Buffer1[i] = 0;
		else
			Buffer1[i] = 1 / VectorSigm[i];
	}

	for (int i = 0; i < N_razm; i++)
	{
		for (int j = 0; j < M_razm; j++)
		{
			if (i == j)
				TranspMatrSigm[i * M_razm + j] = Buffer1[i];
			else
				TranspMatrSigm[i * M_razm + j] = 0;
		}
	}

	//умножаем V на транспонированную Sigma
	for (int i = 0; i < N_razm; i++)
	{
		for (int j = 0; j < M_razm; j++)
		{
			for (int k = 0; k < M_razm; k++)
			{
				Buffer2[i * N_razm + j] += MatrV[i * N_razm + k] * TranspMatrSigm[k * N_razm + j];
			}
		}
	}

	//умножаем результат предыдущего действия на транспонированную матрицу U (определение псевдообратной матрицы A-крест)
	for (int i = 0; i < N_razm; i++)
	{
		for (int j = 0; j < M_razm; j++)
		{
			for (int k = 0; k < M_razm; k++)
			{
				Buffer3[i * N_razm + j] += Buffer2[i * N_razm + k] * Buffer[k * N_razm + j];
			}
		}
	}

	out << "\n\nВектор Сигма:\n\n";
	for (int i = 0; i < N_razm; i++)
	{
		out << setprecision(3) << VectorSigm[i] << "\t";
	}

	out << "\n\nМатрица Сигма:\n\n";
	for (int i = 0; i < N_razm; i++)
	{
		for (int j = 0; j < M_razm; j++)
		{
			out << setprecision(3) << MatrSigm[i * M_razm + j] << "\t";
		}
		out << "\n";
	}

	out << "\nТранспонированная матрица Сигма:\n\n";
	for (int i = 0; i < N_razm; i++)
	{
		for (int j = 0; j < M_razm; j++)
		{
			out << setprecision(3) << TranspMatrSigm[i * M_razm + j] << "\t";
		}
		out << "\n";
	}

	out << "\n\nМатрица U:\n\n";
	for (int i = 0; i < N_razm; i++)
	{
		for (int j = 0; j < M_razm; j++)
		{
			out << setprecision(3) << MatrU[i * N_razm + j] << "\t";
		}
		out << "\n";
	}

	out << "\nМатрица V:\n\n";
	for (int i = 0; i < N_razm; i++)
	{
		for (int j = 0; j < M_razm; j++)
		{
			out << setprecision(3) << MatrV[i * N_razm + j] << "\t";
		}
		out << "\n";
	}

	out << "\nПроизведение VSigma^-1:\n\n";
	for (int i = 0; i < N_razm; i++)
	{
		for (int j = 0; j < M_razm; j++)
		{
			out << setprecision(3) << Buffer2[i * N_razm + j] << "\t";
		}
		out << "\n";
	}

	out << "\nПсевдообратная матрица Мура-Пенроуза (A+):\n\n";
	for (int i = 0; i < N_razm; i++)
	{
		for (int j = 0; j < M_razm; j++)
		{
			out << setprecision(3) << Buffer3[i * N_razm + j] << "\t";
		}
		out << "\n";
	}

	//число обусловленности

	float cond_rand = 0;
	cond_rand = VectorSigm[0] / VectorSigm[N_razm - 1];

	out << "\nЧисло обусловленности матрицы А: " << cond_rand;

	for (int i = 0; i < N_razm; i++)
	{
		for (int j = 0; j < M_razm; j++)
		{
			x_psevdo[i] += Buffer3[i * N_razm + j] * b[j];
		}
	}

	out << "\n\n*************** РЕШЕНИЕ ***************";

	out << "\n\nРешение СЛАУ методом обратной матрицы:\n\n";
	for (int i = 0; i < N_razm; i++)
	{
		out << setprecision(3) << x[i] << '\n';
	}

	out << "\n\nРешение X через псевдообратную матрицу:\n\n";
	for (int i = 0; i < N_razm; i++)
	{
		out << setprecision(3) << x_psevdo[i] << "\n";
	}

	float nevyazki_obr = 0;
	float nevyazki_psevdo = 0;
	float* sum_ax_obr = new float[N_razm];
	float* sum_ax_psevdo = new float[N_razm];
	for (int i = 0; i < N_razm; i++)
	{
		sum_ax_obr[i] = 0;
		sum_ax_psevdo[i] = 0;
		for (int j = 0; j < M_razm; j++)
		{
			sum_ax_obr[i] += A[i][j] * x[j];
			sum_ax_psevdo[i] += A[i][j] * x_psevdo[j];
		}
	}

	for (int i = 0; i < N_razm; i++)
	{
		nevyazki_obr += (b[i] - sum_ax_obr[i]) * (b[i] - sum_ax_obr[i]);
		nevyazki_psevdo += (b[i] - sum_ax_psevdo[i]) * (b[i] - sum_ax_psevdo[i]);
	}
	nevyazki_obr /= N_razm;
	nevyazki_psevdo /= N_razm;

	nevyaz_obr = nevyazki_obr;
	nevyaz_psevdo = nevyazki_psevdo;

	out.close();

	CFile file;
	file.Open(L"result.txt", CFile::modeRead);
	CStringA str;
	LPSTR pBuf = str.GetBuffer(file.GetLength() + 1);
	file.Read(pBuf, file.GetLength() + 1);
	pBuf[file.GetLength()] = NULL;
	CStringA decodedText = str;
	file.Close();
	str.ReleaseBuffer();

	result_part = "";
	result_part = str;

	UpdateData(FALSE);

	for (int i = 0; i < N_razm; ++i)
	{
		delete[] A[i];
	}
	delete[] A;

	for (int i = 0; i < N_razm; ++i)
	{
		delete[] Aobr[i];
	}
	delete[] Aobr;

	delete[] MatrA;
	delete[] MatrU;
	delete[] MatrSigm;
	delete[] MatrV;
	delete[] Buffer;
	delete[] Buffer1;
	delete[] Buffer2;
	delete[] Buffer3;
	delete[] VectorSigm;
	delete[] TranspMatrSigm;
	delete[] b;
	delete[] x;
	delete[] x_psevdo;
}


void Cnecorrtask3Dlg::OnBnClickedSignal()
{
	// TODO: добавьте свой код обработчика уведомлений
	UpdateData(TRUE);

	//memset(Signal, 0, t_sign * sizeof(float));
	
	if (gaussov.GetCheck())
	{
		for (int i = 0; i < t_sign; i++)
		{
			Signal[i] = GaussSignal(i);
		}
	}

	if (polygarm.GetCheck())
	{
		for (int i = 0; i < t_sign; i++)
		{
			Signal[i] = PolySignal(i);
		}
	}

	if (expZatuh.GetCheck())
	{
		for (int i = 0; i < t_sign; i++)
		{
			Signal[i] = ZatuxSignal(i);
		}
	}

	Mashtab(Signal, t_sign, &mnx, &mxx);
	PererisovkaX(mnx, mxx);

	PicDcSign->SelectObject(&signal_pen);
	PicDcSign->MoveTo(DOTS(0, Signal[0]));;

	for (int i = 0; i < t_sign; i++)
	{
		PicDcSign->LineTo(DOTS(i, Signal[i]));
	}
}


void Cnecorrtask3Dlg::OnBnClickedReshenie()
{
	// TODO: добавьте свой код обработчика уведомлений

	//строим автокорреляционную матрицу
	double* rxx = new double[poryadokAKM];
	memset(rxx, 0, poryadokAKM * sizeof(double));

	double summ;

	for (int m = 0; m < poryadokAKM; m++)
	{
		summ = 0;
		for (int k = 0; k < poryadokAKM - m; k++)
		{
			summ += Signal[k] * Signal[k + m];
		}
		rxx[m] = summ / (poryadokAKM - m);
	}

	ofstream out("signal.txt");

	out << "\n\n АКП: \n\n";
	for (int j = 0; j < poryadokAKM; j++)
	{
		out << setprecision(1) << rxx[j] << "   ";
	}

	out << endl;

	int P = poryadokAKM * poryadokAKM;
	double* Rij = new double[P];
	int index = 0;

	for (int i = 0; i < poryadokAKM; i++)
	{
		for (int j = 0; j < poryadokAKM; j++)
		{
			if (i - j >= 0)
			{
				Rij[i * poryadokAKM + j] = rxx[i - j];
			}
			else
			{
				index = poryadokAKM + (i - j);
				Rij[i * poryadokAKM + j] = rxx[index];
			}
		}
	}

	out << "\n\nКорреляционная матрица:\n\n";
	for (int j = 0; j < poryadokAKM; j++)
	{
		out << setprecision(1) << Rij[j] << "\t";
	}
	out << endl;

	double* MatrRij = new double[P];
	double* MatrU = new double[P];
	memset(MatrU, 0, P * sizeof(double));
	double* MatrV = new double[P];
	memset(MatrV, 0, P * sizeof(double));
	double* VectorSigm = new double[P];
	memset(VectorSigm, 0, P * sizeof(double));
	double* MatrSigm = new double[P];
	double* TranspMatrSigm = new double[P];

	double* Buffer = new double[P];
	double* Buffer1 = new double[poryadokAKM];
	double* Buffer2 = new double[P];
	double* Buffer3 = new double[P];

	for (int i = 0; i < poryadokAKM; i++)
	{
		VectorSigm[i] = 0;
		Buffer1[i] = 0;
		for (int j = 0; j < poryadokAKM; j++)
		{
			MatrRij[i * poryadokAKM + j] = Rij[i * poryadokAKM + j];
			MatrU[i * poryadokAKM + j] = 0;
			MatrV[i * poryadokAKM + j] = 0;
			MatrSigm[i * poryadokAKM + j] = 0;
			TranspMatrSigm[i * poryadokAKM + j] = 0;
			Buffer[i * poryadokAKM + j] = 0;
			Buffer2[i * poryadokAKM + j] = 0;
			Buffer3[i * poryadokAKM + j] = 0;
		}
	}

	svd_hestenes(poryadokAKM, poryadokAKM, MatrRij, MatrU, MatrV, VectorSigm);

	out << "\n\n Матрица после SVD: \n\n";
	for (int j = 0; j < poryadokAKM; j++)
	{
		out << setprecision(3) << MatrRij[j] << "   ";
	}

	out << endl;

	//транспонируем матрицу U
	for (int i = 0; i < poryadokAKM; i++)
	{
		for (int j = 0; j < poryadokAKM; j++)
		{
			Buffer[i * poryadokAKM + j] = MatrU[i + j * poryadokAKM];
		}
	}

	for (int i = 0; i < poryadokAKM; i++)
	{
		for (int j = 0; j < poryadokAKM; j++)
		{
			if (i == j)
				MatrSigm[i * poryadokAKM + j] = VectorSigm[i];
			else
				MatrSigm[i * poryadokAKM + j] = 0;
		}
	}

	//транспонируем матрицу Sigma
	for (int i = 0; i < poryadokAKM; i++)
	{
		Buffer1[i] = 1 / VectorSigm[i];
	}

	for (int i = 0; i < poryadokAKM; i++)
	{
		for (int j = 0; j < poryadokAKM; j++)
		{
			if (i == j)
				TranspMatrSigm[i * poryadokAKM + j] = Buffer1[i];
			else
				TranspMatrSigm[i * poryadokAKM + j] = 0;
		}
	}

	//умножаем V на транспонированную Sigma
	for (int i = 0; i < poryadokAKM; i++)
	{
		for (int j = 0; j < poryadokAKM; j++)
		{
			for (int k = 0; k < poryadokAKM; k++)
			{
				Buffer2[i * poryadokAKM + j] += MatrV[i * poryadokAKM + k] * TranspMatrSigm[k * poryadokAKM + j];
			}
		}
	}

	//умножаем результат предыдущего действия на транспонированную матрицу U (определение псевдообратной матрицы A-крест)
	for (int i = 0; i < poryadokAKM; i++)
	{
		for (int j = 0; j < poryadokAKM; j++)
		{
			for (int k = 0; k < poryadokAKM; k++)
			{
				Buffer3[i * poryadokAKM + j] += Buffer2[i * poryadokAKM + k] * Buffer[k * poryadokAKM + j];
			}
		}
	}

	out << "Вектор Сигма:\n\n";
	for (int i = 0; i < poryadokAKM; i++)
	{
		out << setprecision(3) << VectorSigm[i] << "\t";
	}

	Mashtab(VectorSigm, poryadokAKM, &mny, &mxy);
	PererisovkaY(-0.1 * mxy, mxy);

	PicDcSobstv->SelectObject(&signal_pen);
	PicDcSobstv->MoveTo(DOTSSOBSTV(0, VectorSigm[0]));

	for (int i = 0; i < poryadokAKM; ++i)
	{
		PicDcSobstv->LineTo(DOTSSOBSTV(i, VectorSigm[i]));
	}

	double* Vector = new double[poryadokAKM];
	for (int i = 0; i < poryadokAKM; i++)
	{
		Vector[i] = 0;
		for (int j = 0; j < poryadokAKM; j++)
		{
			Vector[i] = Buffer[i + number * j];
		}
	}

	Mashtab(Vector, poryadokAKM, &mnsv, &mxsv);
	PererisovkaSV(mnsv, mxsv);

	PicDcSv->SelectObject(&signal_pen);
	PicDcSv->MoveTo(DOTSSV(0, Vector[0]));

	for (int i = 0; i < poryadokAKM; ++i)
	{
		PicDcSv->LineTo(DOTSSV(i, Vector[i]));
	}

	out << "Собственный вектор:\n\n";
	for (int i = 0; i < poryadokAKM; i++)
	{
		out << Vector[i] << "\t";
	}

	out.close();
	delete[] rxx;
	delete[] Rij;

	delete[] Vector;
	delete[] MatrRij;
	delete[] MatrU;
	delete[] MatrSigm;
	delete[] MatrV;
	delete[] Buffer;
	delete[] Buffer1;
	delete[] Buffer2;
	delete[] Buffer3;
	delete[] VectorSigm;
	delete[] TranspMatrSigm;
}
