﻿
// necorrtask3Dlg.h: файл заголовка
//

#pragma once


// Диалоговое окно Cnecorrtask3Dlg
class Cnecorrtask3Dlg : public CDialogEx
{
// Создание
public:
	Cnecorrtask3Dlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_NECORRTASK3_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedRezult();
	afx_msg void OnBnClickedSignal();
	afx_msg void OnBnClickedReshenie();

	int M_razm;
	int N_razm;
	double determ;
	double nevyaz_obr;
	double nevyaz_psevdo;
	CString result_part;

	double det(double** T, UINT32 N);
	int svd_hestenes(int m_m, int n_n, double* a, double* u, double* v, double* sigma);

	CWnd* PicWndSign;
	CDC* PicDcSign;
	CRect PicSign;

	CWnd* PicWndSobstv;
	CDC* PicDcSobstv;
	CRect PicSobstv;

	CWnd* PicWndSv;
	CDC* PicDcSv;
	CRect PicSv;

	double A1;		double omega1;		double phase1;		double sigma1;		double t1;
	double A2;		double omega2;		double phase2;		double sigma2;		double t2;
	double A3;		double omega3;		double phase3;		double sigma3;		double t3;
	
	int poryadokAKM;
	int t_sign;
	CButton polygarm;
	CButton gaussov;
	CButton expZatuh;

	//Переменные для работы с масштабом
	double xpx = 0, ypx = 0,			//коэфициенты пересчета
		xminx = -15, xmaxx = t_sign,			//максисимальное и минимальное значение х 
		yminx = -1, ymaxx = 50;			//максисимальное и минимальное значение y
	double mnx = -5, mxx = 50;					//коэффициенты масштабирования

	double xpy = 0, ypy = 0,			//коэфициенты пересчета
		xminy = -15, xmaxy = t_sign,			//максисимальное и минимальное значение х 
		yminy = -1, ymaxy = 50;			//максисимальное и минимальное значение y
	double mny = -5, mxy = 50;					//коэффициенты масштабирования

	double xpsv = 0, ypsv = 0,			//коэфициенты пересчета
		xminsv = -15, xmaxsv = t_sign,			//максисимальное и минимальное значение х 
		yminsv = -1, ymaxsv = 50;			//максисимальное и минимальное значение y
	double mnsv = -5, mxsv = 50;					//коэффициенты масштабирования

	//объявление ручек
	CPen osi_pen;		// ручка для осей
	CPen setka_pen;		// для сетки
	CPen signal_pen;		// для графика функции
	CPen signal2_pen;		// для графика функции

	afx_msg void PererisovkaX(double y_min, double y_max);
	afx_msg void PererisovkaY(double y_min, double y_max);
	afx_msg void PererisovkaSV(double y_min, double y_max);
	void Mashtab(double arr[], int dim, double* mmin, double* mmax);

	double PolySignal(int t);
	double GaussSignal(int t);
	double ZatuxSignal(int t);

	double* Signal = new double[t_sign];
	int number;
};
